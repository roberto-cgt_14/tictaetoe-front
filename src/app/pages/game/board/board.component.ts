import { DialogComponent } from './../dialog/dialog.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
squares: any[];
xIsNext: boolean;
winner: String;
gameOn: boolean = true;
  constructor(private dialogRef:MatDialog) { }

  ngOnInit() {
    this.newGame();
  }
  startGame(){
    this.gameOn=true;
  }
  newGame(){

    this.squares = Array(9).fill(null);

    this.winner=null;
    this.xIsNext=true;
  }
  get player(){
    return this.xIsNext ? 'X' : 'O';
  }
  makeMove(idx:number){
    if (!this.squares[idx]) {
      this.squares.splice(idx,1,this.player);
      this.xIsNext= !this.xIsNext;
    }
    this.winner=this.calculateWinner();
  }
  calculateWinner() {
    const lines = [
      [0,1,2],
      [3,4,5],
      [6,7,8],
      [0,3,6],
      [1,4,7],
      [2,5,8],
      [0,4,8],
      [2,4,6]
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a,b,c] = lines[i];
      if (this.squares[a] && this.squares[a] === this.squares[b]&& this.squares[a]===this.squares[c]) {

        const dialogRef = this.dialogRef.open(DialogComponent,{width:'350px',data: 'El ganador es '+this.squares[a]})
        dialogRef.afterClosed().subscribe(res=>{
         if (res) {
          this.newGame();
         }
         else{
           this.newGame();
           this.gameOn=false;
         }
        })


        return this.squares[a];
      }

    }
    return null;
  }

}
